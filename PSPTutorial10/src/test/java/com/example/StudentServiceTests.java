package com.example;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.StudentModel;
import com.example.service.StudentService;

@RunWith(SpringRunner.class) 
@SpringBootTest 
@Transactional 
public class StudentServiceTests  {      
	@Autowired  
	StudentService service;  
	
	@Test 
	public void testSelectAllStudents()  {     
		List<StudentModel> students = service.selectAllStudents(); 
		Assert.assertNotNull("Gagal = student menghasilkan null", students);
		Assert.assertEquals("Gagal - size students tidak sesuai", 5, students.size());
	}
	
	@Test 
	public void testSelectStudent()  {     
		StudentModel students = service.selectStudent("123"); 
		Assert.assertNotNull("Gagal = student menghasilkan null", students);
		Assert.assertEquals("Gagal - Nama student tidak sesuai", "Chanek", students.getName());
		Assert.assertEquals("Gagal - GPA student tidak sesuai", 3.55, students.getGpa(), 0.0);
	}
	
	@Test 
	public void testCreateStudent()  {     
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);  
		// Cek apakah student sudah ada  
		Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));     
		
		// Masukkan ke service  
		service.addStudent(student); 
		
		//Cek apakah student berhasil dimasukkan  
		Assert.assertNotNull("Mahasiswa gagal dimasukkan",service.selectStudent(student.getNpm()));       
	}
	
	@Test 
	public void testDeleteStudent()  {     
		StudentModel student = service.selectStudent("124");  
		
		// Cek apakah student null
		Assert.assertNotNull("Gagal = student menghasilkan null", student);     
		
		// Delete Student  
		service.deleteStudent(student.getNpm());
		
		StudentModel student2 = service.selectStudent("124");
		
		//Cek apakah student berhasil dihapus 
		Assert.assertNotNull("Mahasiswa tidak ditemukan",service.selectStudent(student2.getNpm()));       
	}
	
	@Test 
	public void testUpdateStudent()  {     
		StudentModel student = service.selectStudent("124");  
		
		// Cek apakah student null
		Assert.assertNotNull("Gagal = student menghasilkan null", student);     
		
		//Cek Ip awal student
		Assert.assertEquals("Gagal - GPA student tidak sesuai", 1.7, student.getGpa(), 0.0);
		
		// Ubah GPA  
		student.setGpa(3.55);
		
		service.updateStudent(student);
		
		StudentModel student2 = service.selectStudent("124");
		
		//Cek Ip akhir student
		Assert.assertEquals("Gagal - GPA student tidak sesuai", 3.55, student2.getGpa(), 0.0);     
	}
	
}
